window.onload = init;

function init(){
    const map = new ol.Map({
        view: new ol.View({
            center: [1389880.7546642218, 5144703.885074405],
            zoom: 9,
            maxZoom: 15,
            // minZoom: 5,
            rotation: 0.5
        }),
        target: 'mia-mappa'
    })

    const openStreetMapStandard = new ol.layer.Tile({
        source: new ol.source.OSM(),
        visible: true,
        title: 'OSMStandard'
    })

    const stamenToner = new ol.layer.Tile({
        source: new ol.source.XYZ({
            url: 'https://stamen-tiles.a.ssl.fastly.net/toner/{z}/{x}/{y}.png',
            attributions: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.'
        }),
        visible: false,
        title: 'StamenToner'
    })

    

    const gruppoLayerMappeBase = new ol.layer.Group({
        layers: [
            openStreetMapStandard,
            stamenToner
        ]
    }) 

    map.addLayer(gruppoLayerMappeBase)

    //--------------------------- SWITCHER MAPPE

    const radioSwitcher = document.querySelectorAll('.selettore-ol')
    
    for(let radioInput of radioSwitcher){
        radioInput.addEventListener('change', function(){
            let mappaSelezionata = this.value

            gruppoLayerMappeBase.getLayers().forEach(element => {
                let titoloLivello = element.get('title')
                element.setVisible(titoloLivello === mappaSelezionata)
            });


        })
    }

    // ------------------------ LAYER-------------------------------

   
    const tutti_percorsi = new ol.layer.VectorImage({
        source: new ol.source.Vector({
            url: './vettori/tutti_percorsi.geojson',
            format: new ol.format.GeoJSON()
        }),
        visible: true,
        title: 'tutti_percorsi',
    })
    const percorso_1 = new ol.layer.VectorImage({
        source: new ol.source.Vector({
            url: './vettori/percorso_1.geojson',
            format: new ol.format.GeoJSON()
        }),
        visible: true,
        title: 'percorso_1',
    })
    const percorso_2 = new ol.layer.VectorImage({
        source: new ol.source.Vector({
            url: './vettori/percorso_2.geojson',
            format: new ol.format.GeoJSON()
        }),
        visible: true,
        title: 'percorso_2',
    })
    const percorso_3 = new ol.layer.VectorImage({
        source: new ol.source.Vector({
            url: './vettori/percorso_3.geojson',
            format: new ol.format.GeoJSON()
        }),
        visible: true,
        title: 'percorso_3',
    })
    
    const gruppoPercorsi = new ol.layer.Group({
        layers: [
            percorso_1,
            percorso_2,
            percorso_3,
            tutti_percorsi,
            
        ]
    }) 

    map.addLayer(gruppoPercorsi)


     //--------------------------- SWITCHER PERCORSI

     const radioSwitcher_due = document.querySelectorAll('.selettore-ve')
    
     for(let radioInput_2 of radioSwitcher_due){
         radioInput_2.addEventListener('change', function(){
             let percorsoSelezionato = this.value
 
             gruppoPercorsi.getLayers().forEach(element => {
                 let titoloPercorso = element.get('title')
                 element.setVisible(titoloPercorso === percorsoSelezionato)
             });
 
 
         })
     }
     // ------------------------- AGGIUNTA OVERLAY ------------------

    const overlayHtml = document.querySelector('.overlay-container');
    const overlayLayer = new ol.Overlay({
        element: overlayHtml
    })
    map.addOverlay(overlayLayer)

    const spanOverlayNome = document.getElementById('overlay-nome');
    const spanOverlayInfo = document.getElementById('overlay-info');

    map.on('click', function(evt){
        
        overlayLayer.setPosition(undefined)

        map.forEachFeatureAtPixel(evt.pixel, function(feature, layer){
            let nome = feature.get('percorso')
            let info = feature.get('tipo')

            spanOverlayNome.innerHTML = nome;
            spanOverlayInfo.innerHTML = info;

            overlayLayer.setPosition(evt.coordinate)
        })
    })


}